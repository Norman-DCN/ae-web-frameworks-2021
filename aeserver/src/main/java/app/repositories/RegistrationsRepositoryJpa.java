package app.repositories;

import app.exceptions.ResourceNotFoundException;
import app.models.Registration;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository("persistentRegistrationRepository")
@Transactional
public class RegistrationsRepositoryJpa extends AbstractEntityRepositoryJpa<Registration> {

  public RegistrationsRepositoryJpa() {
    super(Registration.class);
  }

  @Override
  public List<Registration> findAll() {
    return em.createQuery("SELECT registration FROM Registration registration", Registration.class).getResultList();
  }

  @Override
  public Registration findById(long id) throws ResourceNotFoundException {
    return em.find(Registration.class, id);
  }

  @Override
  public Registration save(Registration registration) {
    em.persist(registration);
    return registration;
  }

  @Override
  public boolean deleteById(long id) throws ResourceNotFoundException {
    try {
      em.remove(this.findById(id));
      return true;
    } catch (Exception e){
    e.printStackTrace();
    return false;
  }
}


  @Override
  public Registration update(long id, Registration registration) throws ResourceNotFoundException {
    return em.merge(registration);
  }

  @Override
  public void createRandom() {

  }

  @Override
  public List<Registration> findByQuery(String jpqlName, Object... params) {
    return null;
  }
}

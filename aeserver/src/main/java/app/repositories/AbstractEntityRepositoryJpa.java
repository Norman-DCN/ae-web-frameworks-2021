package app.repositories;

import app.interfaces.EntityRepository;
import app.interfaces.Identifiable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
public abstract class AbstractEntityRepositoryJpa<E extends Identifiable> implements EntityRepository<E> {
  @PersistenceContext
  protected EntityManager em;

  private Class<E> entityClass;

  public AbstractEntityRepositoryJpa(Class<E> entityClass) {
    this.entityClass = entityClass;
    System.out.printf("Created %s<%s>\n", this.getClass().getName(), this.getClass().getSimpleName());
  }
}

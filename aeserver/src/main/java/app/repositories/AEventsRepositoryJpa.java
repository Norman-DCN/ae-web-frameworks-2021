package app.repositories;

import app.exceptions.ResourceNotFoundException;
import app.models.AEvent;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository("persistentAEventsRepository")
public class AEventsRepositoryJpa extends AbstractEntityRepositoryJpa<AEvent> {
  public AEventsRepositoryJpa() {
    super(AEvent.class);
  }

  @Override
  public List<AEvent> findAll() {
    return em.createQuery("SELECT aevent FROM AEvent aevent", AEvent.class).getResultList();
  }

  @Override
  public void createRandom() {
    this.save(AEvent.createRandomEvent());
  }

  @Override
  public AEvent findById(long id) {
    return em.find(AEvent.class, id);
  }

  @Override
  public AEvent save(AEvent aEvent) {
    em.persist(aEvent);
    return aEvent;
  }

  @Override
  public boolean deleteById(long id) {
    try {
      em.remove(this.findById(id));
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  @Override
  public AEvent update(long id, AEvent aEvent) throws ResourceNotFoundException {
    return em.merge(aEvent);
  }

  @Override
  public List<AEvent> findByQuery(String jpqlName, Object... params) {
    // create query based on jpql name
    TypedQuery<AEvent> namedQuery = em.createNamedQuery(jpqlName, AEvent.class);
    // for each param, set the parameter
    for (int i = 0; i < params.length; i++) {
      namedQuery.setParameter(i + 1, params[i]);
    }
    // return result
    return namedQuery.getResultList();


  }
}

package app.repositories;

import app.exceptions.AlreadySavedException;
import app.exceptions.PreConditionFailedException;
import app.exceptions.ResourceNotFoundException;
import app.interfaces.EntityRepository;
import app.models.AEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AEventsRepositoryMock implements EntityRepository<AEvent> {

 private List<AEvent> aEvents = new ArrayList<>();

  public AEventsRepositoryMock() {

    for (int i = 1; i <=7; i++) {
      aEvents.add(AEvent.createRandomEvent());
    }
  }

  public void createRandom() {
    aEvents.add(AEvent.createRandomEvent());
  }

  @Override
  public List<AEvent> findAll() {
    return aEvents;
  }

  @Override
  public AEvent findById(long id) throws ResourceNotFoundException {
    // find and return the AEvent with the specified id.
    for (AEvent event: aEvents) {
      if(event.getId() == id) {
        return event;
      }
    }
    throw new ResourceNotFoundException();
  }

  @Override
  public AEvent save(AEvent aEvent) throws AlreadySavedException {
    if(aEvents.contains(aEvent)) {
        throw new AlreadySavedException("this event has already been saved, please try the PUT request for updating the aevent.");
    } else {
      // add the event
      this.aEvents.add(aEvent);
      return aEvent;
    }
  }

  @Override
  public boolean deleteById(long id) throws ResourceNotFoundException {
    AEvent event = this.findById(id);
    return this.aEvents.remove(event);
  }

  @Override
  public AEvent update(long id, AEvent aEvent) throws PreConditionFailedException, ResourceNotFoundException {
    // retrieve the old aevent.
    AEvent oldAEvent = findById(id);
    // checks if oldAEvent is null or if the id is not the same as in the url request.
    if(oldAEvent == null || aEvent.getId() != id) {
      throw new PreConditionFailedException("AEvent id " + aEvent.getId() + " is not the same as path id (" + id + ")");
    }
    // replace the old aevent with the new aevent, why this is better
    // is because you dont have play with the id in the path and the new object's id.
    aEvents.set(aEvents.indexOf(oldAEvent), aEvent);
    return aEvent;
  }

  @Override
  public List<AEvent> findByQuery(String jpqlName, Object... params) {
    return null;
  }
}

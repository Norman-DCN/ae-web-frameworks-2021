package app.config;

import app.filters.JWTRequestFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Set;

@Configuration
@PropertySource(value = "application.properties", ignoreResourceNotFound = true)

public class APIConfig implements WebMvcConfigurer {

  @Bean
  public JWTRequestFilter filterBean() {
    return new JWTRequestFilter();
  }

  @Value("${jwt.issuer:smnj}")
  private String issuer;
  @Value("${jwt.pass-phrase:Is this the real life or is this just fantasy caught in a landslide no escape from reality open your eyes look up to the skies and see}")
  private String passPhrase;
  @Value("${jwt.duration-of-validity: 3600}")
  private int tokenDurationOfValidity;

  public String getIssuer() {
    return issuer;
  }

  public String getPassPhrase() {
    return passPhrase;
  }

  public int getTokenDurationOfValidity() {
    return tokenDurationOfValidity;
  }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
      .allowCredentials(true)
      .allowedHeaders(HttpHeaders.AUTHORIZATION, HttpHeaders.CONTENT_TYPE)
      .exposedHeaders(HttpHeaders.AUTHORIZATION, HttpHeaders.CONTENT_TYPE)
      .allowedMethods("GET", "POST", "PUT", "DELETE")
      .allowedOrigins("http://localhost:4200");

  }
}

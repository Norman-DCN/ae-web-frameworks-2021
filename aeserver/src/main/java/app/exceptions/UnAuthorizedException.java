package app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnAuthorizedException extends RuntimeException {
  public UnAuthorizedException(String errMsg) {
    super(errMsg);
  }

  public UnAuthorizedException(RuntimeException e) {
    super(e);
  }
}

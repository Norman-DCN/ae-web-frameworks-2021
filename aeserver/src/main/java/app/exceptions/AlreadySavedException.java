package app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.ALREADY_REPORTED)
public class AlreadySavedException extends Exception {
  public AlreadySavedException(String errMsg) {
    super(errMsg);
  }
}

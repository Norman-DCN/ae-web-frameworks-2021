package app.rest;

import app.config.APIConfig;
import app.exceptions.UnAuthorizedException;
import app.models.JWToken;
import app.models.User;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/authenticate")
public class AuthenticateController {


  @Autowired
  APIConfig apiConfig;
//  private EntityRepository<User> userRepo;


  @PostMapping("/login")
  public ResponseEntity<User> autheticateUser(
    @RequestBody ObjectNode singOnInfo,
    HttpServletRequest request)
  {
    String userEmail = singOnInfo.get("eMail").asText();
    String userPassword = singOnInfo.get("passWord").asText();
    String eMailName = userEmail.substring(0, userEmail.indexOf("@") );

    if(eMailName.equals(userPassword)) {
      // create user
      User savedUser = new User(eMailName, userEmail, userPassword, false);
      // fill up the payload
      JWToken jwToken = new JWToken(savedUser.getName(), savedUser.getId(), savedUser.getAdmin());
      // encode the token with the secret stuff
      String token = jwToken.encode(this.apiConfig.getIssuer(), this.apiConfig.getPassPhrase(), this.apiConfig.getTokenDurationOfValidity());
      return ResponseEntity.accepted()
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
        .body(savedUser);
    } else {
      // throw UnAuthorizedException
      throw new UnAuthorizedException("Cannot auth user by email=" + userEmail + " and password=" + userPassword);
    }
  }



}

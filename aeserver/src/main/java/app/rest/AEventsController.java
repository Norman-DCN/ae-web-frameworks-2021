package app.rest;

import app.exceptions.AlreadySavedException;
import app.exceptions.PreConditionFailedException;
import app.exceptions.ResourceNotFoundException;
import app.interfaces.EntityRepository;
import app.models.AEvent;
import app.models.Registration;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/aevents")
public class AEventsController {
  @Qualifier("persistentAEventsRepository")
  @Autowired
  private EntityRepository<AEvent> aeventRepository;
  @Autowired EntityRepository<Registration> registrationRepository;
  @GetMapping
  public List<AEvent> getAllAEvents(
    HttpServletRequest request,
    @RequestParam(required = false) Optional<String> title,
    @RequestParam(required = false) Optional<AEvent.AEventStatus> status,
    @RequestParam(required = false) Optional<Integer> minRegistrations
    ) {
    // check if the request contains more than 1 parameter
    if (request.getParameterMap().size() > 1) {
      throw new PreConditionFailedException("Extra parameters are present");
    }
    else {
      // if there are parameters, check if they are present.
      if(title.isPresent()) {
       return aeventRepository.findByQuery("AEvent_find_by_title", title.get());
      }
      if(status.isPresent()) {
        // check if status exists in enum
        try {
          for (AEvent.AEventStatus aEventStatus : AEvent.AEventStatus.values()) {
            if (aEventStatus.name().equals(status.get().name())) {
              return aeventRepository.findByQuery("AEvent_find_by_status", status.get());
            }
          }
        } catch (PreConditionFailedException e) {
          throw new PreConditionFailedException(" is not a valid aeventstatus value");
        }
      }
      if(minRegistrations.isPresent()) {
        return aeventRepository.findByQuery("AEvent_find_by_minRegistrations", minRegistrations.get());
      }

      // if none are present return all.
      return aeventRepository.findAll();
    }
  }
  @GetMapping("/create/randomEvent")
  public void getCreatedRandomEvent() { aeventRepository.createRandom(); }
  @GetMapping("/summary")
  @JsonView(AEvent.Summary.class)
  public List<AEvent> getAEventsSummary() {
    return aeventRepository.findAll();
  }
  @GetMapping("/{id}")
  public AEvent getById(@PathVariable int id) throws ResourceNotFoundException {
    return aeventRepository.findById(id);
  }
  @PostMapping()
  public ResponseEntity<AEvent> save(@RequestBody AEvent aEvent) throws ResourceNotFoundException, AlreadySavedException {
    AEvent savedAEvent = aeventRepository.save(aEvent);
    URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{id}").buildAndExpand(savedAEvent).toUri();
    return ResponseEntity.created(location).body(savedAEvent);
  }

  @PostMapping("/{aeventId}/register")
  public ResponseEntity<Registration> saveRegistration(@PathVariable int aeventId, @RequestBody LocalDateTime submissionDateTime) throws ResourceNotFoundException, AlreadySavedException {
    AEvent aEvent = aeventRepository.findById(aeventId);
    Registration savedRegistration, createdRegistration;

    if(aEvent.getStatus() == AEvent.AEventStatus.PUBLISHED) {

      createdRegistration = aEvent.createNewRegistration(submissionDateTime);
      savedRegistration = registrationRepository.save(createdRegistration);

      URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{aeventId}/register").buildAndExpand(savedRegistration).toUri();
      return ResponseEntity.created(location).body(savedRegistration);

    } else {
      throw new PreConditionFailedException("AEvent with aeventId=" + aeventId + " is not published.");
    }
  }
  @PutMapping("/{id}")
  public ResponseEntity<AEvent> update(@PathVariable int id, @RequestBody AEvent aEvent) throws PreConditionFailedException, ResourceNotFoundException {
    AEvent editedAEvent = aeventRepository.update(id, aEvent);
    return ResponseEntity.ok(editedAEvent);
  }
  @DeleteMapping("/{id}")
  public boolean deleteById(@PathVariable int id) throws ResourceNotFoundException {
    return aeventRepository.deleteById(id);

  }
}

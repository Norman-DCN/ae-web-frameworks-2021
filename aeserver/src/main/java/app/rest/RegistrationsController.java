package app.rest;

import app.exceptions.AlreadySavedException;
import app.exceptions.PreConditionFailedException;
import app.exceptions.ResourceNotFoundException;
import app.models.AEvent;
import app.models.Registration;
import app.repositories.AEventsRepositoryJpa;
import app.repositories.RegistrationsRepositoryJpa;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/registrations")
public class RegistrationsController {
  @Autowired
  private RegistrationsRepositoryJpa registrationsRepository;
  @GetMapping
  public List<Registration> getAllRegistrations() {
    return registrationsRepository.findAll();
  }

  @GetMapping("/{id}")
  public Registration getById(@PathVariable int id) throws ResourceNotFoundException {
    return registrationsRepository.findById(id);
  }
  @PostMapping()
  public ResponseEntity<Registration> save(@RequestBody Registration registration) throws ResourceNotFoundException, AlreadySavedException {
    Registration savedRegistration = registrationsRepository.save(registration);
    URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{id}").buildAndExpand(savedRegistration).toUri();
    return ResponseEntity.created(location).body(savedRegistration);
  }
  @PutMapping("/{id}")
  public ResponseEntity<Registration> update(@PathVariable int id, @RequestBody Registration registration) throws PreConditionFailedException, ResourceNotFoundException {
    Registration editedRegistration = registrationsRepository.update(id, registration);
    return ResponseEntity.ok(editedRegistration);
  }
  @DeleteMapping("/{id}")
  public boolean deleteById(@PathVariable int id) throws ResourceNotFoundException {
    return registrationsRepository.deleteById(id);
  }
}

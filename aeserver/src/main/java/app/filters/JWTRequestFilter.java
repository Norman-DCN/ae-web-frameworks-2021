package app.filters;

import app.config.APIConfig;
import app.exceptions.UnAuthorizedException;
import app.models.JWToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

public class JWTRequestFilter extends OncePerRequestFilter {
  @Autowired
  APIConfig apiConfig;

  private static final Set<String> PROTECTED_PATHS = Set.of(
    "/aevents",
    "/registrations",
    "/users"
  );

  @Override
  protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
    try {
      String servletPath = httpServletRequest.getServletPath();

      if(HttpMethod.OPTIONS.matches(httpServletRequest.getMethod()) ||
        PROTECTED_PATHS.stream().noneMatch(servletPath::startsWith)) {
        filterChain.doFilter(httpServletRequest, httpServletResponse);
        return;
      }

      JWToken token = null;
      String encryptedToken = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);

      if(encryptedToken != null) {
        encryptedToken = encryptedToken.replace("Bearer ", "");
        token = JWToken.decode(encryptedToken, this.apiConfig.getPassPhrase());
      }
      if(token == null) {
        throw new UnAuthorizedException("You need to login first.");
      }

      httpServletRequest.setAttribute("Token", token);
      filterChain.doFilter(httpServletRequest, httpServletResponse);

    } catch (UnAuthorizedException e) {
      httpServletResponse.sendError(httpServletResponse.SC_UNAUTHORIZED, e.getCause().getMessage());
    }
  }
}

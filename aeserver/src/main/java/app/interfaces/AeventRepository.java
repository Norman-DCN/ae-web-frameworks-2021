package app.interfaces;

import app.exceptions.AlreadySavedException;
import app.exceptions.ResourceNotFoundException;
import app.models.AEvent;

import java.util.List;

public interface AeventRepository {

  List<AEvent> findAll();
  AEvent findById(int id) throws ResourceNotFoundException;
  AEvent save(AEvent aEvent) throws ResourceNotFoundException, AlreadySavedException;
  boolean deleteById(int id) throws ResourceNotFoundException;

  AEvent update(int id, AEvent aEvent) throws ResourceNotFoundException;
}

package app.interfaces;

import app.exceptions.AlreadySavedException;
import app.exceptions.ResourceNotFoundException;
import app.interfaces.Identifiable;

import java.util.List;

public interface EntityRepository<E extends Identifiable> {

  List<E> findAll();
  E findById(long id) throws ResourceNotFoundException;
  E save(E e) throws ResourceNotFoundException, AlreadySavedException;
  boolean deleteById(long id) throws ResourceNotFoundException;
  E update(long id, E e) throws ResourceNotFoundException;
  void createRandom();
  List<E> findByQuery(String jpqlName, Object... params);

}

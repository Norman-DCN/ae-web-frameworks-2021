package app.interfaces;

public interface Identifiable {
  long getId();
  void setId(long id);
}

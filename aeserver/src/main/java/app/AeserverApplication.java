package app;

import app.models.AEvent;
import app.models.Registration;
import app.repositories.AEventsRepositoryJpa;
import app.repositories.RegistrationsRepositoryJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class AeserverApplication implements CommandLineRunner
{
  @Autowired
  private AEventsRepositoryJpa aEventsRepositoryJpa;
  @Autowired
  private RegistrationsRepositoryJpa registrationsRepositoryJpa;


	public static void main(String[] args) {
		SpringApplication.run(AeserverApplication.class, args);
	}

	@Transactional
  @Override
  public void run(String... args) throws Exception {
    System.out.println("Running CommandLine Startup");
    this.setEverythingUp();
  }

  private void setEverythingUp() {
	  List<AEvent> aEvents = aEventsRepositoryJpa.findAll();
	  List<Registration> registrations = registrationsRepositoryJpa.findAll();

	  if(aEvents.size() > 0) return;
    System.out.println("Configuring some initial aEvent data");

    for (int i = 0; i < 9; i++) {
      AEvent aEvent = AEvent.createRandomEvent();
      aEvents.add(aEvent);
    }

    if(registrations.size() > 0) return;
    System.out.println("Configuring some initial registration data");

    for (int i = 0; i < 9; i++) {
      Registration registration = new Registration();
      registration.setPaid(Math.random() > 0.5);
      registration.setTicketCode("AEX" + i*i +"FOD" + i + "X");
      registration.setSubmissionDate(Registration.random(LocalDateTime.now()));
      registrations.add(registration);
    }

    System.out.println("Linking aEvent data with Registration data");

    aEvents.get(3).setRegistrations(List.of(registrations.get(1)));
    aEvents.get(2).setRegistrations(List.of(registrations.get(2)));
    aEvents.get(1).setRegistrations(List.of(registrations.get(3)));

    registrations.get(1).setaEvent(aEvents.get(3));
    registrations.get(2).setaEvent(aEvents.get(2));
    registrations.get(3).setaEvent(aEvents.get(1));

    System.out.println("Persisting aEvent data and Registration data");

    for (AEvent aevent: aEvents) {
      aEventsRepositoryJpa.save(aevent);
    }
    for (Registration registration: registrations) {
      registrationsRepositoryJpa.save(registration);
    }
  }
}

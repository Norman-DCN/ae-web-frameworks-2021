package app.models;

import app.interfaces.Identifiable;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
@Entity
@NamedQueries({
  @NamedQuery(name = "find_all_AEvents",
    query = "SELECT a FROM AEvent AS a "),

  @NamedQuery(name = "AEvent_find_by_status",
    query = "SELECT a FROM AEvent AS a WHERE a.status= ?1 "),

  @NamedQuery(name = "AEvent_find_by_title",
    query = "SELECT a FROM AEvent AS a WHERE a.title LIKE CONCAT('%',?1,'%') "),

  @NamedQuery(name = "AEvent_find_by_minRegistrations",
    query = "SELECT a as total FROM AEvent AS a  WHERE a.registrations.size = ?1 ")
})
public class AEvent implements Identifiable {

  public Registration createNewRegistration(LocalDateTime submissionDateTime) {
    Registration registration = new Registration();

    registration.setaEvent(this);
    registration.setSubmissionDate(submissionDateTime);
    registration.setPaid(Math.random() > 0.5);
    registration.setTicketCode("AEX" + randomInteger(5000, 1) * randomInteger(5, 1) + "FOD" + randomInteger(5000, 1) + "X");
    return registration;
  }

  public enum AEventStatus {
    DRAFT,
    PUBLISHED,
    CANCELED,
  }
  private static int count = 0;
  @JsonView(Summary.class)
  @Id
  @GeneratedValue(generator = "sequence-generator")
  @GenericGenerator(
    name = "sequence-generator",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
      @Parameter(name = "sequence_name", value = "aevent_sequence"),
      @Parameter(name = "initial_value", value = "20001"),
      @Parameter(name = "increment_size", value = "1")
    }
  )
  private long id = 20001;
  @JsonView(Summary.class)
  private String title;
  private LocalDateTime start;
  private LocalDateTime end;
  private String description;
  @JsonView(Summary.class)
  @Enumerated(EnumType.STRING)
  private AEventStatus status;
  private Boolean isTicketed;
  private Double participationFee;
  private int maxParticipants;

  //removing a event and the registrations of the event
  @OneToMany(mappedBy = "aEvent", cascade = CascadeType.REMOVE)
  @JsonBackReference
  private List<Registration> registrations = new ArrayList<>();

  public AEvent() {
//    this.setId(getId() + AEvent.count++);
    this.setTitle("A fantasic backend aEvent");
  }

  public AEvent(String title) {
    this.title = title;
  }

 public static int getCount() {
    return count;
  }

  public static void setCount(int count) {
    AEvent.count = count;
  }

  public long getId() {
    return id;
  }

  @Override
  public void setId(long id) { this.id = id; }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public LocalDateTime getStart() {
    return start;
  }

  public void setStart(LocalDateTime start) {
    this.start = start;
  }

  public LocalDateTime getEnd() {
    return end;
  }

  public void setEnd(LocalDateTime end) {
    this.end = end;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public AEventStatus getStatus() {
    return status;
  }

  public void setStatus(AEventStatus status) {
    this.status = status;
  }

  public Boolean getTicketed() {
    return isTicketed;
  }

  public void setTicketed(Boolean ticketed) {
    isTicketed = ticketed;
  }

  public Double getParticipationFee() {
    return participationFee;
  }

  public void setParticipationFee(Double participationFee) {
    this.participationFee = participationFee;
  }

  public int getMaxParticipants() {
    return maxParticipants;
  }

  public void setMaxParticipants(int maxParticipants) {
    this.maxParticipants = maxParticipants;
  }

  public List<Registration> getRegistrations() {
    return registrations;
  }

  public void setRegistrations(List<Registration> registrations) {
    this.registrations = registrations;
  }

  public static int randomInteger(int max, int min) {
    return (int) (Math.floor(Math.random() * max) + min);
  }
  public static LocalDateTime random(LocalDateTime start) {
    LocalDateTime now = LocalDateTime.now();
    int year = 60 * 60 * 24 * 365;
    LocalDateTime end = now.plusSeconds(randomInteger(-2 * year, 2 * year));// +- 2 years;

    if (start.isBefore(end)) {
      end = start.plusSeconds(randomInteger(-2* year, 2 * year));
    }

    return end;
  }

  public static LocalDateTime random() {
    return random(LocalDateTime.now());
  }

  public static AEventStatus randomEnum() {
    int pick = new Random().nextInt(AEventStatus.values().length);
    return AEventStatus.values()[pick];
  }

  public static AEvent createRandomEvent() {
    AEvent aevent = new AEvent();

    aevent.setTicketed(Math.random() > 0.5);

    if(aevent.getTicketed()) {
      aevent.setParticipationFee((double) ((randomInteger(100000, 1)) / 100));
      aevent.setMaxParticipants(randomInteger(100,1));
    }

    aevent.setStart(random());
    aevent.setEnd(random(aevent.getStart()));
    aevent.setDescription("");
    aevent.setStatus(randomEnum());

    return aevent;
  }


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AEvent{");
    sb.append("id=").append(id);
    sb.append("title=").append(title).append('\'');
    sb.append(", start=").append(start);
    sb.append(", end=").append(end);
    sb.append(", description='").append(description).append('\'');
    sb.append(", status=").append(status);
    sb.append(", isTicketed=").append(isTicketed);
    sb.append(", participationFee=").append(participationFee);
    sb.append(", maxParticipants=").append(maxParticipants);
    sb.append(", registrations=").append(registrations);
    sb.append('}');
    return sb.toString();
  }

  public class Summary {
  }
}

package app.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//@Entity
public class User {

  private static long Count = 1;
//  @Id
//  @GeneratedValue(generator = "sequence-generator")
//  @GenericGenerator(
//    name = "sequence-generator",
//    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
//    parameters = {
//
//      @Parameter(name = "sequence_name", value = "user_sequence"),
//      @Parameter(name = "initial_value", value = "90001"),
//      @Parameter(name = "increment_size", value = "1")
//    }
//  )
  private long id = 90000;
  private String name;
  private String eMail;
  private String hashedPassWord;
  private boolean admin;

  public User(String name, String email, String pass, boolean isAdmin) {
    this.id = this.getId() + Count++;
    this.admin = isAdmin;
    this.eMail = email;
    this.hashedPassWord = pass;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String geteMail() {
    return eMail;
  }

  public void seteMail(String eMail) {
    this.eMail = eMail;
  }

  public String getHashedPassWord() {
    return hashedPassWord;
  }

  public void setHashedPassWord(String hashedPassWord) {
    this.hashedPassWord = hashedPassWord;
  }

  public Boolean getAdmin() {
    return admin;
  }

  public void setAdmin(Boolean admin) {
    this.admin = admin;
  }
}

package app.models;

import app.interfaces.Identifiable;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

//1 reg = 1 AEvent
// 1 AEvent = many reg

@Entity
public class Registration implements Identifiable {
  @Id
  @GeneratedValue(generator = "sequence-generator")
  @GenericGenerator(
    name = "sequence-generator",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {

      @org.hibernate.annotations.Parameter(name = "sequence_name", value = "registration_sequence"),
      @org.hibernate.annotations.Parameter(name = "initial_value", value = "100001"),
      @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
    }
  )
  private long id;
  private String ticketCode;
  private boolean paid;
  private LocalDateTime submissionDate;

  @ManyToOne
  @JsonManagedReference
  private AEvent aEvent;

  public Registration() {

  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTicketCode() {
    return ticketCode;
  }

  public void setTicketCode(String ticketCode) {
    this.ticketCode = ticketCode;
  }

  public boolean isPaid() {
    return paid;
  }

  public void setPaid(boolean paid) {
    this.paid = paid;
  }

  public LocalDateTime getSubmissionDate() {
    return submissionDate;
  }

  public void setSubmissionDate(LocalDateTime submissionDate) {
    this.submissionDate = submissionDate;
  }

  public AEvent getaEvent() {
    return this.aEvent;
  }

  public void setaEvent(AEvent aEvent) {
    this.aEvent = aEvent;
  }
  public static int randomInteger(int max, int min) {
    return (int) (Math.floor(Math.random() * max) + min);
  }

  public static LocalDateTime random(LocalDateTime start) {
    LocalDateTime now = LocalDateTime.now();
    int year = 60 * 60 * 24 * 365;
    LocalDateTime end = now.plusSeconds(randomInteger(-2 * year, 2 * year));// +- 2 years;

    if (start.isBefore(end)) {
      end = start.plusSeconds(randomInteger(-2* year, 2 * year));
    }

    return end;
  }
}

import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { AEvent, AEventStatus } from 'src/app/models/a-event';

@Component({
  selector: 'app-detail2',
  templateUrl: './detail2.component.html',
  styleUrls: ['./detail2.component.scss']
})
export class Detail2Component implements OnInit {
  statuses = Object.keys(AEventStatus).map(k => k);
  @Input() aEvent:AEvent;
  @Output() AEventChange:EventEmitter<AEvent> = new EventEmitter<AEvent>(); 
 
  constructor() { }

  ngOnInit(): void {
    
  }
  
  remove(){
    this.AEventChange.emit(this.aEvent);
  }
}

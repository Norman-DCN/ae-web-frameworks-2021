import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AEventsSbService} from '../../../services/a-events-sb.service';
import {AEvent} from '../../../models/a-event';

@Component({
  selector: 'app-overview5',
  templateUrl: './overview5.component.html',
  styleUrls: ['./overview5.component.scss']
})
export class Overview5Component implements OnInit {


selectedAEventId: number = -1;
  public aevents: AEvent[] = [];

  onIdChange(id : number) {
    this.selectedAEventId = id;
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public aEventService: AEventsSbService,
  ) {}

  ngOnInit(): void {
    this.allEvents();
  }

  allEvents() {
    this.aEventService.restGetAEvents().subscribe(
      (data) => {
        this.aevents = data;
        this.aevents.forEach(aevent => {
          this.aevents[this.aevents.indexOf(aevent)] = AEvent.trueCopy(aevent);
        });
        console.log(this.aevents);
      },
      (error) => {
       alert('Error: Status ' + error.status + '-' + error.error);
      }
    );
  }

  pushRandomEventToList() {
    this.aEventService.addRandomEvent().subscribe((data) => {
      this.allEvents();
    });
  }
  onSelect(eId:number) {
    this.selectedAEventId = eId;
    this.router.navigate([eId], {relativeTo: this.activatedRoute});
  }

}

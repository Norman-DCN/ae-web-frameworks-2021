import { Component, OnInit } from '@angular/core';
import { AEvent } from 'src/app/models/a-event';

@Component({
  selector: 'app-overview2',
  templateUrl: './overview2.component.html',
  styleUrls: ['./overview2.component.scss']
})
export class Overview2Component implements OnInit {
  public aEvents: AEvent[];

  SelectedAEvent: AEvent;

  constructor() { }

  ngOnInit(): void {
    this.aEvents = [];
    for (let i = 0; i < 9; i++) {

      this.addRandomEvent();

    }
  }

  showDetails(aEvent: AEvent) {
    this.SelectedAEvent = Object.assign(aEvent);
  }

  addRandomEvent() {
    
    let event = AEvent.createRandomAEvent();
    
    this.aEvents.push(event);
    this.showDetails(event);

    console.log(this.aEvents);
  }

  remove(event:AEvent) {
    this.aEvents.splice(this.aEvents.indexOf(event),1);
    this.SelectedAEvent = null;
  }
}

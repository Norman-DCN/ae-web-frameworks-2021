import { Component, Input, EventEmitter, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { AEvent, AEventStatus } from 'src/app/models/a-event';
import { AEventsService } from 'src/app/services/a-events.service';

@Component({
  selector: 'app-detail3',
  templateUrl: './detail3.component.html',
  styleUrls: ['./detail3.component.scss'],
})
export class Detail3Component implements OnInit {
  
  aEventId: number;
  @Output('editedAEventId') aEventIdChange:EventEmitter<number> = new EventEmitter<number>();

  @Input()
  get editedAEventId() {
    return this.aEventId;
  }
  set editedAEventId(id : number) {
    this.aEventId = id;
    this.aEventIdChange.emit(this.aEventId);
  }
  
  
  statuses = Object.keys(AEventStatus).map(k => k);

  public aEvent:AEvent;

  constructor(public aeventService:AEventsService = new AEventsService()) { 
            
  }

  ngOnInit(): void {
    
    // creates a copy of the value provided. combining this with getters and setters
    // you cant see the private properties which makes this safer.
    this.aEvent = Object.create(this.aeventService.findById(this.editedAEventId));    
  }
  
  ngOnChanges(): void {
    this.aEvent = Object.create(this.aeventService.findById(this.editedAEventId));
  }

  clear() {
    
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.aEvent.setDescription = null;
      this.aEvent.setEnd = null;
      this.aEvent.setStart = null;
      this.aEvent.setFee = null;
      this.aEvent.setIsTicketed = false;
      this.aEvent.setMaxParticipants = null;
      this.aEvent.setTitle = null;
      this.aEvent.setStatus = null;
    }
  }

  save(): void {  
    this.aeventService.save(this.aEvent);
    this.editedAEventId = -1; // unselect event.
  }

  delete(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.aeventService.deleteById(this.aEvent.getId);
      this.editedAEventId = -1; // unselect event.
    }
    
  }

  reset(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.ngOnInit();
    }
  }
  cancel(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.ngOnInit();
      this.editedAEventId = -1;
    }
  }

  isDisabled() {

    let tempEvent = this.aeventService.findById(this.editedAEventId);
    let hasChanges = false;

    if(this.aEvent.getFee != tempEvent.getFee) {
      hasChanges = true;
    }

    if(this.aEvent.getDescription != tempEvent.getDescription) {
      hasChanges = true;
    }
    
    if(this.aEvent.getIsTicketed != tempEvent.getIsTicketed) {
      hasChanges = true;
    }

    if(this.aEvent.getTitle != tempEvent.getTitle) {
      hasChanges = true;
    }
    
    if(this.aEvent.getMaxParticipants != tempEvent.getMaxParticipants) {
      hasChanges = true;
    }
    if(this.aEvent.getStatus != tempEvent.getStatus) {
      hasChanges = true;
    }
    
    if(hasChanges == true) {
      return true;
    } else {

      return false;
    }
  }
}

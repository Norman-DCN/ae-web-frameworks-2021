import { Component, Input, EventEmitter, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { AEvent, AEventStatus } from 'src/app/models/a-event';
import {AEventsSbService} from '../../../services/a-events-sb.service';
import {Overview5Component} from '../overview5/overview5.component';

@Component({
  selector: 'app-detail5',
  templateUrl: './detail5.component.html',
  styleUrls: ['./detail5.component.scss'],
})
export class Detail5Component implements OnInit {

  aEventId: number;
  get editedAEventId() {
    return this.aEventId;
  }
  set setEditedAEventId(id: number) {
    this.aEventId = id;
  }

  statuses = Object.keys(AEventStatus).map(k => k);
  public aEvent: AEvent;
  public tempEvent: AEvent;
  private childParamsSubscription: Subscription = null;

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      public aeventService: AEventsSbService,
      private overview5Component: Overview5Component
    ) {}

    ngOnInit(): void {
    // creates a copy of the value provided. combining this with getters and setters
    // you cant see the private properties which makes this safer.
    this.childParamsSubscription =
      this.route.params.subscribe((params: Params) => {

        this.setEditedAEventId = params['id'] || -1;
        this.aeventService.findById(this.editedAEventId).subscribe(
          (data) => {
            this.aEvent = AEvent.trueCopy(data);
          },
          (error) => {
            alert("error!");
          }
        );
      });
    }

    ngOnDestroy() {
      this.childParamsSubscription && this.childParamsSubscription.unsubscribe();
    }

  clear() {

    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.aEvent.setDescription = null;
      this.aEvent.setEnd = null;
      this.aEvent.setStart = null;
      this.aEvent.setFee = null;
      this.aEvent.setIsTicketed = false;
      this.aEvent.setMaxParticipants = null;
      this.aEvent.setTitle = null;
      this.aEvent.setStatus = null;
    }
  }

  save(): void {
    this.aeventService.findById(this.aEvent.getId).subscribe(
      (data) => {
        if(AEvent.trueCopy(data).getId === this.aEvent.getId) {
          this.aeventService.restPutAEvent(this.aEvent).subscribe(() => {});
        } else {
          this.aeventService.restPostAEvent(this.aEvent).subscribe(() => {});
        }
      },
      (error) => {
        alert("error!");
      }
    );

    this.router.navigateByUrl('events/overview-5', {relativeTo: this.route.parent});
  }

  delete(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.aeventService.restDeleteAEvent(this.aEvent.getId);
      // navigate to parent, then call the allEvents so that the list gets updated.
      this.router.navigateByUrl('events/overview-5', {relativeTo: this.route.parent}).then(() => {
        this.overview5Component.allEvents();
      });
    }

  }

  reset(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.ngOnInit();
    }
  }
  cancel(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.ngOnInit();
      // navigate to parent, then call the allEvents so that the list gets updated.
      this.router.navigateByUrl('events/overview-5', {relativeTo: this.route.parent}).then(() => {
        this.overview5Component.allEvents();
      });    }
  }

  /**
   * FIXME: this function is broken.
   * When you call findById() and subscribe to it, it keeps
   * calling the method in the
   * backend which creates an infinite loop.
   */
  // isDisabled() {
  //
  //   this.aeventService.findById(this.editedAEventId).subscribe(
  //     (data) => this.tempEvent = AEvent.trueCopy(data),
  //     (error) => alert('error!')
  //   );
  //   let hasChanges = false;
  //   if(this.aEvent.getFee != this.tempEvent.getFee) {
  //     hasChanges = true;
  //   }
  //
  //   if(this.aEvent.getDescription != this.tempEvent.getDescription) {
  //     hasChanges = true;
  //   }
  //
  //   if(this.aEvent.getIsTicketed != this.tempEvent.getIsTicketed) {
  //     hasChanges = true;
  //   }
  //
  //   if(this.aEvent.getTitle != this.tempEvent.getTitle) {
  //     hasChanges = true;
  //   }
  //
  //   if (this.aEvent.getMaxParticipants != this.tempEvent.getMaxParticipants) {
  //     hasChanges = true;
  //   }
  //   if(this.aEvent.getStatus != this.tempEvent.getStatus) {
  //     hasChanges = true;
  //   }
  //
  //   return hasChanges === true;
  // }
}

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AEventsService } from 'src/app/services/a-events.service';

@Component({
  selector: 'app-overview4',
  templateUrl: './overview4.component.html',
  styleUrls: ['./overview4.component.scss']
})
export class Overview4Component implements OnInit {


selectedAEventId: number = -1;

  onIdChange(id : number) {
    this.selectedAEventId = id;
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public aEventService: AEventsService = new AEventsService()
  ) {}

  ngOnInit(): void {

  }

  onSelect(eId:number) {
    this.selectedAEventId = eId;
    this.router.navigate([eId], {relativeTo: this.activatedRoute});
  }

}

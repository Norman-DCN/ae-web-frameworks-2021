import { Component, Input, EventEmitter, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { AEvent, AEventStatus } from 'src/app/models/a-event';
import { AEventsService } from 'src/app/services/a-events.service';

@Component({
  selector: 'app-detail4',
  templateUrl: './detail4.component.html',
  styleUrls: ['./detail4.component.scss'],
})
export class Detail4Component implements OnInit {

  aEventId: number;
  get editedAEventId() {
    return this.aEventId;
  }
  set setEditedAEventId(id: number) {
    this.aEventId = id;
    // this.aEventIdChange.emit(this.aEventId);
  }

  statuses = Object.keys(AEventStatus).map(k => k);
  public aEvent: AEvent;
  private childParamsSubscription: Subscription = null;

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      public aeventService:AEventsService = new AEventsService()
    ) {}

    ngOnInit(): void {
    // creates a copy of the value provided. combining this with getters and setters
    // you cant see the private properties which makes this safer.

    this.childParamsSubscription =
      this.route.params.subscribe((params: Params) => {

        this.setEditedAEventId = params['id'] || -1;
        this.aEvent = Object.create(this.aeventService.findById(this.editedAEventId));

      });
    }

    ngOnDestroy() {
      this.childParamsSubscription && this.childParamsSubscription.unsubscribe();
    }

  clear() {

    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.aEvent.setDescription = null;
      this.aEvent.setEnd = null;
      this.aEvent.setStart = null;
      this.aEvent.setFee = null;
      this.aEvent.setIsTicketed = false;
      this.aEvent.setMaxParticipants = null;
      this.aEvent.setTitle = null;
      this.aEvent.setStatus = null;
    }
  }

  save(): void {
    this.aeventService.save(this.aEvent);
    this.router.navigateByUrl('events/overview-4', {relativeTo: this.route.parent});
  }

  delete(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.aeventService.deleteById(this.aEvent.getId);
      this.router.navigateByUrl('events/overview-4', {relativeTo: this.route.parent});
    }

  }

  reset(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.ngOnInit();
    }
  }
  cancel(): void {
    let result = confirm("are you sure to discard unsaved changes?");

    if (result === true) {
      this.ngOnInit();
      this.router.navigateByUrl('events/overview-4', {relativeTo: this.route.parent});
    }
  }

  isDisabled() {

    let tempEvent = this.aeventService.findById(this.editedAEventId);
    let hasChanges = false;

    if(this.aEvent.getFee != tempEvent.getFee) {
      hasChanges = true;
    }

    if(this.aEvent.getDescription != tempEvent.getDescription) {
      hasChanges = true;
    }

    if(this.aEvent.getIsTicketed != tempEvent.getIsTicketed) {
      hasChanges = true;
    }

    if(this.aEvent.getTitle != tempEvent.getTitle) {
      hasChanges = true;
    }

    if(this.aEvent.getMaxParticipants != tempEvent.getMaxParticipants) {
      hasChanges = true;
    }
    if(this.aEvent.getStatus != tempEvent.getStatus) {
      hasChanges = true;
    }

    if(hasChanges == true) {
      return true;
    } else {

      return false;
    }
  }
}

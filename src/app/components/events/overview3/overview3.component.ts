import { Component, Input, OnInit } from '@angular/core';
import { AEventsService } from 'src/app/services/a-events.service';

@Component({
  selector: 'app-overview3',
  templateUrl: './overview3.component.html',
  styleUrls: ['./overview3.component.scss']
})
export class Overview3Component implements OnInit {

  
selectedAEventId: number = -1;
  
  onIdChange(id : number) {
    this.selectedAEventId = id;
  }
  
  constructor(public aEventService:AEventsService = new AEventsService()) {}
  
  ngOnInit(): void {
  
  }
}

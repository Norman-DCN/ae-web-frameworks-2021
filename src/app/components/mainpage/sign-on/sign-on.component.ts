import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionSbService} from '../../../services/SessionSbService';

@Component({
  selector: 'app-sign-on',
  templateUrl: './sign-on.component.html',
  styleUrls: ['./sign-on.component.scss']
})
export class SignOnComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private sessionSbService: SessionSbService
  ) {
    this.form = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  submitForm() {
    this.sessionSbService.signIn(this.form.get('email').value, this.form.get('password').value);
  }
}

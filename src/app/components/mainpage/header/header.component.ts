import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public date: string = this.dateString('en', 'long', '2-digit', 'long', 'numeric');

  constructor () {}

  ngOnInit(): void {
  }

  public dateString(lang: string, weekDay: string, day: string, month: string, year: string) {

    const date = new Date;
    const formattedYear = new Intl.DateTimeFormat(lang, { year: year }).format(date);
    const formattedMonth = new Intl.DateTimeFormat(lang, { month: month }).format(date);
    const formattedDay = new Intl.DateTimeFormat(lang, { day: day }).format(date);
    const formattedWeekDay = new Intl.DateTimeFormat(lang, {weekday: weekDay}).format(date);

    return `${formattedWeekDay}, ${formattedDay} ${formattedMonth} ${formattedYear}`;
  }

}

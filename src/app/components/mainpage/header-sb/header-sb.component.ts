import { Component, OnInit } from '@angular/core';
import {SessionSbService} from '../../../services/SessionSbService';

@Component({
  selector: 'app-header-sb',
  templateUrl: './header-sb.component.html',
  styleUrls: ['./header-sb.component.scss']
})
export class HeaderSbComponent implements OnInit {

  public date: string = this.dateString('en', 'long', '2-digit', 'long', 'numeric');

  constructor(public sessionSbService: SessionSbService) {}

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  public dateString(lang: string, weekDay: string, day: string, month: string, year: string) {

    // tslint:disable-next-line:new-parens
    const date = new Date;
    const formattedYear = new Intl.DateTimeFormat(lang, { year: year }).format(date);
    const formattedMonth = new Intl.DateTimeFormat(lang, { month: month }).format(date);
    const formattedDay = new Intl.DateTimeFormat(lang, { day: day }).format(date);
    const formattedWeekDay = new Intl.DateTimeFormat(lang, {weekday: weekDay}).format(date);

    return `${formattedWeekDay}, ${formattedDay} ${formattedMonth} ${formattedYear}`;
  }

}

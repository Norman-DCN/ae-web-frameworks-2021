import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  public url: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
    console.log(this.router);
    this.url = this.router.url;
  }

}

import {
  Injectable
} from '@angular/core';
import {
  AEvent
} from '../models/a-event';

@Injectable({
  providedIn: 'root'
})
export class AEventsService {

  public aEvents: AEvent[];

  constructor() {
    this.aEvents = [];

    for (let i = 0; i < 9; i++) {

      this.addRandomEvent();

    }
  }

  findAll(): AEvent[] {
    // return the list of all aEvents.
    return this.aEvents;
  }

  findById(id: number): AEvent {
    // find and return the AEvent with the specified id.
    let event = this.findAll().find(event => event.getId == id);
    if (event) return event;
    else {
      // return null if none found.
      return null;
    }

  }

  save(aEvent: AEvent): AEvent {
    // find the event
    let event = this.findById(aEvent.getId);

    if (event) {
      // replace the aEvent with the same id with the provided data.
      this.findAll()[this.aEvents.indexOf(event)] = aEvent;
      // return the old, replaced aEvent.
      return aEvent;

    } else {
      // add the new aEvent if none existed and return null.
      this.findAll().push(aEvent)
      return null;
    }
  }

  deleteById(id: number): AEvent {
    // get the event by id.
    let rEvent = this.findById(id);
    // get the index of the identified event.
    let index = this.findAll().indexOf(rEvent);

    // remove the identified AEvent from the array.
    // splice zoekt dmv van de index. Vervolgens haalt hij de rijen weg
    this.findAll().splice(index, 1);

    if (rEvent) {
      // return the removed instance.
      return rEvent;
    } else {
      // return null if none found.
      return null;
    }
  }

  addRandomEvent() {
    this.aEvents.push(AEvent.createRandomAEvent());
  }
}

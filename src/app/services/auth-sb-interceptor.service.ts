import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SessionSbService} from './SessionSbService';

@Injectable({
  providedIn: 'root'
})
export class AuthSbInterceptorService implements HttpInterceptor {

  constructor(private session: SessionSbService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.session.getTokenFromSessionStorage();

    if (token != null) {
      const request = req.clone({headers: req.headers.set('Authorization', token)});
      return next.handle(request);
    } else {
      return next.handle(req);
    }
  }
}

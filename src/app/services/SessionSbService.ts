import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {shareReplay} from 'rxjs/operators';
import {User} from '../models/user';
import {AbstractControl} from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class SessionSbService {
  public readonly BACKEND_AUTH_URL = 'http://localhost:8084/authenticate';
  public readonly AE_SB_AUTH_TOKEN_NAME = 'AE_SB_AUTH_TOKEN';
  public currentUserName: string = null;
  private message: any;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.getTokenFromSessionStorage();
  }

  isAuthenticated(): boolean {
    return this.currentUserName != null;
  }

  signIn(email: string, password: string, targetUrl?: string): any {
    console.log('login ' + email + '/' + password);
    const signInResponse =
      this.http.post<HttpResponse<User>>(this.BACKEND_AUTH_URL + '/login',
        {
          eMail: email,
          passWord: password
        },
        // opties van request om alles terug te krijgen
        {
          observe: 'response',
          headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
      ).pipe(shareReplay(1));

    signInResponse
      .subscribe(
        response => {
          console.log(response);
          this.saveTokenIntoSessionStorage(
            response.headers.get('Authorization'),
            (response.body as unknown as User).name
          );
          this.router.navigate([targetUrl || '/']);
        },
        error => {
          console.log(error);
          this.saveTokenIntoSessionStorage(null, null);
          this.setError(error.error.message);
        }
      );
    return signInResponse;
  }

  signOut(): any {
    this.saveTokenIntoSessionStorage(null, null);
  }
  getTokenFromSessionStorage(): any {
    let token = sessionStorage.getItem(this.AE_SB_AUTH_TOKEN_NAME);
    if (token == null) {
      token = localStorage.getItem(this.AE_SB_AUTH_TOKEN_NAME);
      sessionStorage.setItem(this.AE_SB_AUTH_TOKEN_NAME, token);
    }else {
      // split the token each / delimiter
      let tokenParts = token.split('/');
      this.currentUserName = tokenParts[1];
      return tokenParts[0];
    }
    return null;
  }

  private saveTokenIntoSessionStorage(authToken: string, name: any): any {
    const namedToken = authToken + '/' + name;
    const oldSessionToken = sessionStorage.getItem(this.AE_SB_AUTH_TOKEN_NAME);

    if (oldSessionToken === namedToken) {
      return;
    }
    console.log('New token: ', namedToken);
    if (authToken == null) {
      this.currentUserName = null;
      const oldLocalToken = localStorage.getItem(this.AE_SB_AUTH_TOKEN_NAME);
      sessionStorage.removeItem(this.AE_SB_AUTH_TOKEN_NAME);
      if (oldSessionToken === oldLocalToken) {
        localStorage.removeItem(this.AE_SB_AUTH_TOKEN_NAME);
      }
    } else {
      this.currentUserName = name;
      sessionStorage.setItem(this.AE_SB_AUTH_TOKEN_NAME, namedToken);
      localStorage.setItem(this.AE_SB_AUTH_TOKEN_NAME, namedToken);
    }
  }

  private setError(message?: any): any {
    this.message = message;
  }
}

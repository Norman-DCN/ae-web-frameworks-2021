import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AEvent} from '../models/a-event';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AEventsSbService {
  private SERVER_URL = 'http://localhost:8084';

  constructor(private httpClient: HttpClient) {
    this.restGetAEvents();
  }

  restGetAEvents(): Observable<AEvent[]> {
    const url = `${this.SERVER_URL}/aevents`;
    return this.httpClient.get<AEvent[]>(url);
  }

  findById(aEventId: number): Observable<AEvent> {
    const url = `${this.SERVER_URL}/aevents/${aEventId}`;
    return this.httpClient.get<AEvent>(url);
  }

  restPostAEvent(aEvent): Observable<AEvent> {
    const url = `${this.SERVER_URL}/aevents`;
    return this.httpClient.post<AEvent>(url, aEvent);
  }
  restPutAEvent(aEvent): Observable<AEvent> {
    const url = `${this.SERVER_URL}/aevents/${aEvent.id}`;
    return this.httpClient.put<AEvent>(url, aEvent);
  }
  restDeleteAEvent(aEventId): void {
    const url = `${this.SERVER_URL}/aevents/${aEventId}`;
    this.httpClient.delete(url).subscribe(() => {});
  }
  addRandomEvent() {
    const url = `${this.SERVER_URL}/aevents/create/randomEvent`;
    return this.httpClient.get(url);
  }
}

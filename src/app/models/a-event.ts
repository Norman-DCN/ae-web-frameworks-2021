export class AEvent {
    private static count = 0;
    private id: number = 20001;
    private title: string;
    private start: Date;
    private end: Date;
    private description: string;
    private status: AEventStatus;
    private isTicketed: boolean;
    private participationFee: number;
    private maxParticipants: number;

    static trueCopy(aEvent: AEvent): AEvent {
      return (aEvent == null ? null : Object.assign(new AEvent(), aEvent));
    }
    constructor() {

        /**
         * The reason the event gets created in the constructor,
         * is because when a new instantation is being invoked,
         * this constructor gets called, it wouldn't be logical to do this
         * in the createRandomAEvent method,
         * since that method can just return a new object of this class.
         */
        this.setId = this.getId + AEvent.count++;
        this.setTitle = `My awesome event ${this.getId}`;
        this.setStatus = randomEnum(AEventStatus);
        this.setIsTicketed = Math.random() > 0.5;

        if(this.getIsTicketed === true) {
            // divide by 100 to turn the integer into a double with two decimals.
            this.setFee = (randomInteger(100000, 1)) / 100;
            this.setMaxParticipants = randomInteger(100, 1);
        }

        let now = new Date();

        // the reason for so many new Date objects, is because we need to set the date with the date of now.
        //
        let startDate = new Date(new Date().setDate(now.getDate() + randomInteger(31, 1)));
        let endDate = new Date(new Date().setDate(startDate.getDate() + randomInteger(31, 1)));

        // randomize hours.
        startDate.setHours(now.getHours() + randomInteger(23, 2));
        endDate.setHours(now.getHours() + randomInteger(23, 2));
        // randomize minutes.
        startDate.setMinutes(30);
        endDate.setMinutes(30);

        this.setStart = startDate;
        this.setEnd = endDate;
    }
    public static createRandomAEvent(): AEvent {

        return new AEvent();
    }

    public set setId (id: number) {
        this.id = id;
    }
    public get getId(): number {
        return this.id;
    }
    public set setTitle (title: string) {
        this.title = title;
    }

    public get getTitle (): string {
        return this.title;
    }

    public set setDescription (description: string) {
        this.description = description;
    }

    public get getDescription (): string {
        return this.description;
    }

    public set setStatus (status: AEventStatus) {
        this.status = status;
    }

    public get getStatus (): AEventStatus {
        return this.status;
    }

    public set setIsTicketed (isTicketed: boolean) {
        this.isTicketed = isTicketed;
    }
    public get getIsTicketed (): boolean {
        return this.isTicketed;
    }

    public set setFee(fee: number) {
        this.participationFee = fee;
    }

    public get getFee(): number {
        return this.participationFee;
    }
    public set setMaxParticipants(participants: number) {
        this.maxParticipants = participants;
    }

    public get getMaxParticipants(): number {
        return this.maxParticipants;
    }

    public set setStart(date:Date) {
        this.start = date;
    }

    public get getStart(): Date {
        return this.start;
    }

        public set setEnd(date:Date) {
        this.end = date;
    }

    public get getEnd(): Date {
        return this.end;
    }
}
/**
 * @param anEnum AEventStatus
 * @returns random enum value
 */
function randomEnum<AEventStatus>(anEnum: AEventStatus): AEventStatus[keyof AEventStatus] {
    const enumValues = (Object.values(anEnum) as unknown) as AEventStatus[keyof AEventStatus][];
    const randomIndex = Math.floor(Math.random() * enumValues.length);
    return enumValues[randomIndex];
}

/**
 * @description generates a random integer between min and max.
 * @param max maximal number
 * @param min minimal number
 * @returns { number } random number
 */
function randomInteger(max:number, min:number):number {
    return Math.floor(Math.random() * max) + min;
}

export enum AEventStatus {
    Draft = "DRAFT",
    Published = "PUBLISHED",
    Canceled = "CANCELED",
}

export class User {
  name: string;
  private eMail: string;
  private hashedPassWord: string;
  private admin: boolean;

  constructor(name, email, pass, admin) {
    this.admin = admin;
    this.eMail = email;
    this.name = name;
    this.hashedPassWord = pass;
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Overview1Component } from './components/events/overview1/overview1.component';
import { Overview2Component } from './components/events/overview2/overview2.component';
import { Overview3Component } from './components/events/overview3/overview3.component';
import { Overview4Component } from './components/events/overview4/overview4.component';
import { HomeComponent } from './components/mainpage/home/home.component';
import { ErrorComponent } from './components/mainpage/error/error.component';
import { Detail4Component } from './components/events/detail4/detail4.component';
import {Detail5Component} from './components/events/detail5/detail5.component';
import {Overview5Component} from './components/events/overview5/overview5.component';
import {SignOnComponent} from './components/mainpage/sign-on/sign-on.component';
import {Detail51Component} from './components/events/detail51/detail51.component';

const routes: Routes = [
  // redirects
  {path: '', redirectTo: 'home', pathMatch: 'full'}, // sooo als een route verkeerd gaat of je vult een route die niet bestaat wat gebeurd er dan
  // normal routes
  {path: 'home', component: HomeComponent},
  {path: 'login', component: SignOnComponent},
  {path: 'events/overview-1', component: Overview1Component},
  {path: 'events/overview-2', component: Overview2Component},
  {path: 'events/overview-3', component: Overview3Component},
  {path: 'events/overview-4', component: Overview4Component, children: [{path: ':id', component: Detail4Component}]},
  {path: 'events/overview-5', component: Overview5Component, children: [{path: ':id', component: Detail5Component}]},
  {path: 'events/overview-51', component: Overview5Component, children: [{path: ':id', component: Detail51Component}]},
  {path: '**', component: ErrorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

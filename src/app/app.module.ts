import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/mainpage/header/header.component';
import { HomeComponent } from './components/mainpage/home/home.component';
import { NavBarComponent } from './components/mainpage/nav-bar/nav-bar.component';
import { Overview1Component } from './components/events/overview1/overview1.component';
import { Overview2Component } from './components/events/overview2/overview2.component';
import { Detail2Component } from './components/events/detail2/detail2.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Overview3Component } from './components/events/overview3/overview3.component';
import { Detail3Component } from './components/events/detail3/detail3.component';
import { ErrorComponent } from './components/mainpage/error/error.component';
import { Overview4Component } from './components/events/overview4/overview4.component';
import { Detail4Component } from './components/events/detail4/detail4.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {Overview5Component} from './components/events/overview5/overview5.component';
import {Detail5Component} from './components/events/detail5/detail5.component';
import { HeaderSbComponent } from './components/mainpage/header-sb/header-sb.component';
import { SignOnComponent } from './components/mainpage/sign-on/sign-on.component';
import { NavBarSbComponent } from './components/mainpage/nav-bar-sb/nav-bar-sb.component';
import {AuthSbInterceptorService} from './services/auth-sb-interceptor.service';
import { Detail51Component } from './components/events/detail51/detail51.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    NavBarComponent,
    Overview1Component,
    Overview2Component,
    Detail2Component,
    Overview3Component,
    Detail3Component,
    ErrorComponent,
    Overview4Component,
    Detail4Component,
    Overview5Component,
    Detail5Component,
    HeaderSbComponent,
    SignOnComponent,
    NavBarSbComponent,
    Detail51Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthSbInterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
